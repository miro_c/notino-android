package com.md.notinoandroid.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.md.notinoandroid.NotinoApplication
import com.md.notinoandroid.R
import com.md.notinoandroid.adapter.ProductsAdapter
import com.md.notinoandroid.api.AppStateEnum
import com.md.notinoandroid.entity.Product
import com.md.notinoandroid.glide.GlideImageLoader
import com.md.notinoandroid.glide.ImageLoader
import com.md.notinoandroid.livedata.NotinoViewModel
import com.md.notinoandroid.livedata.NotinoViewModelFactory
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch

class FragmentList: Fragment(), ProductsAdapter.OnClickListener {

    private val parentJob = Job()
    private val uiScope = CoroutineScope(Dispatchers.Main + parentJob)

    private lateinit var toolBar: Toolbar
    private lateinit var infoText: TextView
    private lateinit var progress: ProgressBar
    private lateinit var productRecycler: RecyclerView

    private val notinoViewModel: NotinoViewModel by viewModels {
        NotinoViewModelFactory((context?.applicationContext as NotinoApplication).repository)
    }

    private val imageLoader: ImageLoader by lazy {
        GlideImageLoader(requireContext())
    }

    private val productAdapter by lazy {
        ProductsAdapter(this@FragmentList, imageLoader)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        toolBar = view.findViewById(R.id.toolbar)
        (activity as AppCompatActivity).setSupportActionBar(toolBar)
        (activity as AppCompatActivity).getSupportActionBar()?.setDisplayShowTitleEnabled(false)

        infoText = view.findViewById(R.id.info_text)
        progress = view.findViewById(R.id.loading)
        productRecycler = view.findViewById(R.id.product_list)

        productRecycler.apply {
            adapter = productAdapter
            layoutManager = GridLayoutManager(context, 2, RecyclerView.VERTICAL, false)
        }

        uiScope.launch {
            initProductList()
        }
    }

    override fun onItemClick(product: Product) {
        // TODO
    }

    private fun initProductList() {
        notinoViewModel.getStatusLiveData().observe(viewLifecycleOwner) { result ->

            /** Show / Hide loading indicator **/
            handleLoadingState(result.state)

            when (result.state) {
                AppStateEnum.STATE_SUCCESS -> {
                    result.productResponseList?.let { list ->
                        productAdapter.showList(list)
                    }
                }

                AppStateEnum.STATE_ERROR -> {
                    result.message?.let { handleErrorState(result.state, it) }
                }

                else -> {}
            }
        }
    }

    private fun handleLoadingState(state: AppStateEnum){
        progress.visibility = if(state != AppStateEnum.STATE_LOADING) View.GONE else View.VISIBLE
    }

    private fun handleErrorState(state: AppStateEnum, message: String){
        infoText.visibility = if(state != AppStateEnum.STATE_ERROR) View.GONE else View.VISIBLE
        infoText.text = message
    }

}