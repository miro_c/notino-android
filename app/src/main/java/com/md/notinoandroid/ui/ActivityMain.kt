package com.md.notinoandroid.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentContainerView
import com.md.notinoandroid.R


class ActivityMain : AppCompatActivity() {

    private var fragmentIsAdded: Boolean = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        if (savedInstanceState == null) {
            val fragmentList = FragmentList()
            navigateToFragment(fragmentList)
        }
    }

    override fun onBackPressed(){
        super.onBackPressed()

        clearFragmentFlag()
    }

    /** public method to call for fragment navigation in app **/
    fun navigateToFragment(fragment: Fragment) {
        findViewById<FragmentContainerView>(R.id.fragment_container)?.let { frameLayout ->
            setupNavigationTypeFragment(frameLayout, fragment)
        }
    }

    private fun setupNavigationTypeFragment(
        frameLayout: FragmentContainerView,
        fragment: Fragment
    ) {
        val existingFragment = supportFragmentManager.findFragmentById(R.id.fragment_container)
        if (existingFragment != null) {
            if(!fragmentIsAdded){
                replaceFragmentType(frameLayout, fragment)
            }

        } else {
            addFragmentType(frameLayout, fragment)
        }
    }

    private fun addFragmentType(frameLayout: FragmentContainerView, fragment: Fragment){
        supportFragmentManager.beginTransaction()
            .add(frameLayout.id, fragment)
            .commit()
    }

    private fun replaceFragmentType(frameLayout: FragmentContainerView, fragment: Fragment){
        fragmentIsAdded = true

        supportFragmentManager.beginTransaction()
            .replace(frameLayout.id, fragment)
            .addToBackStack(null)
            .commit()
    }

    private fun clearFragmentFlag(){
        fragmentIsAdded = false
    }

}