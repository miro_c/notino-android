package com.md.notinoandroid.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.md.notinoandroid.dao.NotinoDao
import com.md.notinoandroid.data.Converters
import com.md.notinoandroid.entity.Product
import com.md.notinoandroid.utils.DATABASE_NAME

@Database(entities = [Product::class], version = 1)
@TypeConverters(Converters::class)
abstract class NotinoRoomDatabase : RoomDatabase() {

    abstract fun notinoDao(): NotinoDao

    companion object {

        @Volatile
        private var INSTANCE: NotinoRoomDatabase? = null

        fun getDatabase(
            context: Context
        ): NotinoRoomDatabase {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    NotinoRoomDatabase::class.java,
                    DATABASE_NAME
                ).build()
                INSTANCE = instance
                instance
            }
        }
    }
}