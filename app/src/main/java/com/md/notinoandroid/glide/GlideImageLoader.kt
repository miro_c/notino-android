package com.md.notinoandroid.glide

import android.content.Context
import androidx.appcompat.widget.AppCompatImageView
import com.bumptech.glide.Glide
import com.md.notinoandroid.R

class GlideImageLoader(private val context: Context) : ImageLoader {
    override fun loadImage(imageUrl: String, imageView: AppCompatImageView) {
        Glide.with(context)
            .load(imageUrl)
            .error(R.drawable.ic_launcher_background)
            .dontAnimate()
            .into(imageView);
    }
}