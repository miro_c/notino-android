package com.md.notinoandroid.glide

import androidx.appcompat.widget.AppCompatImageView

interface ImageLoader {
    fun loadImage(imageUrl: String, imageView: AppCompatImageView)
}