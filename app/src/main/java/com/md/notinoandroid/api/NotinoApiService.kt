package com.md.notinoandroid.api

import retrofit2.Response
import retrofit2.http.GET

interface NotinoApiService {

    @GET(UrlConstant.URL_ENDPOINT)
    suspend fun getProducts(): Response<VpProduct>
}