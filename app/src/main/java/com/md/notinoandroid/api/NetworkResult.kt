package com.md.notinoandroid.api

import com.md.notinoandroid.entity.Product

enum class AppStateEnum {
    STATE_LOADING, STATE_SUCCESS, STATE_ERROR
}

sealed class NetworkResult(
    val state: AppStateEnum = AppStateEnum.STATE_LOADING,
    val message: String? = null,
    val productResponseList: List<Product>? = null
) {
    class Success(
        message: String?,
        productResponseList: List<Product>
    ) : NetworkResult(state = AppStateEnum.STATE_SUCCESS, message, productResponseList)

    class Error(errorMessage: String?) :
        NetworkResult(state = AppStateEnum.STATE_ERROR, errorMessage)

    class Loading() : NetworkResult(state = AppStateEnum.STATE_LOADING)
}