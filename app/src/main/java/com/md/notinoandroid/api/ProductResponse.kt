package com.md.notinoandroid.api

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class VpProduct(
    @Json(name = "vpProductByIds")
    val productResponses: List<ProductResponse>
)

data class ProductResponse(
    val productId: Long?,
    val brand: Brand?,
    val attributes: Attribute?,
    val annotation: String?,
    val masterId: Long?,
    val url: String?,
    val orderUnit: String?,
    val price: Price?,
    val imageUrl: String?,
    val name: String?,
    val productCode: String?,
    val reviewSummary: ReviewSummary?,
    val stockAvailability: StockAvailability?
)

data class Brand(
    val id: String?,
    val name: String?
)

data class Attribute(
    @Json(name = "New") val new: Boolean?,
    @Json(name = "Action") val action: Boolean?,
    @Json(name = "DifferentPackaging") val differentPackaging: Boolean?,
    @Json(name = "Master") val master: Boolean?,
    @Json(name = "AirTransportDisallowed") val airTransportDisallowed: Boolean?,
    @Json(name = "PackageSize") val packageSize: PackageSize?,
    @Json(name = "FreeDelivery") val freeDelivery: Boolean?
)

data class PackageSize(
    val height: Int?,
    val width: Int?,
    val depth: Int?
)

data class Price(
    val value: Int?,
    val currency: String?
)

data class ReviewSummary(
    val score: Int?,
    val averageRating: String?
)

data class StockAvailability(
    val code: String?,
    val count: Int?
)

