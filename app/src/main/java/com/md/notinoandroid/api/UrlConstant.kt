package com.md.notinoandroid.api

object UrlConstant {
    const val BASE_URL = "https://my-json-server.typicode.com/"
    const val URL_ENDPOINT = "cernfr1993/notino-assignment/db"
    const val IMG_URL_PREFIX = "https://i.notino.com/detail_zoom/"
}