package com.md.notinoandroid.livedata

import androidx.lifecycle.*
import com.md.notinoandroid.api.NetworkResult
import com.md.notinoandroid.api.NotinoApi
import com.md.notinoandroid.entity.Product
import com.md.notinoandroid.repository.NotinoRepository
import com.md.notinoandroid.utils.EntityParser.Companion.getEntityListParsed
import com.md.notinoandroid.utils.ioThread
import kotlinx.coroutines.launch
import java.io.IOException

class NotinoViewModel(private val repository: NotinoRepository) : ViewModel() {

    private val _status = MutableLiveData<NetworkResult>()
    private val status: LiveData<NetworkResult> = _status

    init {
        loadNotinoProducts()
    }

    private fun loadNotinoProducts() {

        /** Init loading state **/
        setLoadingState()

        viewModelScope.launch {
            try {
                /** Http initial request for load products **/
                val httpResult = NotinoApi.retrofitService.getProducts()
                if (httpResult.isSuccessful) {
                    val productList = httpResult.body()?.productResponses
                    val parsedList = productList?.let { getEntityListParsed(it) }

                    if (parsedList != null) {
                        ioThread {
                            /** Save loaded products to Room Database **/
                            insertAllProductsToDb(parsedList)
                        }
                    }

                } else {
                    _status.postValue(NetworkResult.Error(httpResult.message()))
                }

            } catch (e: IOException) {
                _status.postValue(NetworkResult.Error(e.message))
            }
        }
    }

    private fun setLoadingState(){
        viewModelScope.launch {
            _status.postValue(NetworkResult.Loading())
        }
    }

    private fun insertAllProductsToDb(parsedList: List<Product>) {
        try {
            repository.insertProductList(parsedList)
        } finally {
            getAllProductsFromDb()
        }
    }

    private fun getAllProductsFromDb() {
        viewModelScope.launch {
            val data = repository.allProducts()
            if (!data.isEmpty()) {
                _status.postValue(NetworkResult.Success("Data loaded successfully", data))
            } else {
                _status.postValue(NetworkResult.Error("Data load Error"))
            }
        }
    }

    /** public method for use this ViewModel **/
    fun getStatusLiveData(): LiveData<NetworkResult> {
        return status
    }

}

class NotinoViewModelFactory(private val repository: NotinoRepository) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(NotinoViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            return NotinoViewModel(repository) as T
        }
        throw IllegalArgumentException("Unknown ViewModel class")
    }
}