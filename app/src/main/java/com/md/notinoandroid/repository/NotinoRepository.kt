package com.md.notinoandroid.repository

import com.md.notinoandroid.dao.NotinoDao
import com.md.notinoandroid.entity.Product

class NotinoRepository(private val notinoDao: NotinoDao) {

    suspend fun allProducts(): List<Product> {
        return notinoDao.getAllProducts()
    }

    fun insertProductList(data: List<Product>): List<Long> {
        return notinoDao.insertData(data)
    }
}