package com.md.notinoandroid.utils

import com.md.notinoandroid.api.ProductResponse
import com.md.notinoandroid.entity.*
import kotlinx.coroutines.delay

class EntityParser {

    companion object {

        fun getEntityListParsed(productList: List<ProductResponse>): List<Product> {
            val productListParsed: List<Product> = productList.map {
                Product(
                    it.productId,
                    Brand(it.brand?.id, it.brand?.name),
                    Attribute(
                        it.attributes?.new,
                        it.attributes?.action,
                        it.attributes?.differentPackaging,
                        it.attributes?.master,
                        it.attributes?.airTransportDisallowed,
                        PackageSize(
                            it.attributes?.packageSize?.height,
                            it.attributes?.packageSize?.width,
                            it.attributes?.packageSize?.depth
                        ),
                        it.attributes?.freeDelivery
                    ),
                    it.annotation,
                    it.masterId,
                    it.url,
                    it.orderUnit,
                    Price(it.price?.value, it.price?.currency),
                    it.imageUrl,
                    it.name,
                    it.productCode,
                    ReviewSummary(it.reviewSummary?.score, it.reviewSummary?.averageRating),
                    StockAvailability(it.stockAvailability?.code, it.stockAvailability?.count)
                )
            }

            return productListParsed
        }
    }
}