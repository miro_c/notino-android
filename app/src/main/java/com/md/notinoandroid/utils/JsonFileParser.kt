package com.md.notinoandroid.utils

import android.content.Context
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import com.squareup.moshi.Types
import java.io.IOException
import java.io.InputStream

class JsonFileParser {

    companion object {
        fun inputStreamToString(inputStream: InputStream): String {
            try {
                val bytes = ByteArray(inputStream.available())
                inputStream.read(bytes, 0, bytes.size)
                return String(bytes)
            } catch (e: IOException) {
                return ""
            }
        }

        inline fun <reified T> getListOfObjectFromJson(context: Context, jsonFileName: String): T? {
            val moshi: Moshi = Moshi.Builder().build()
            val listType = Types.newParameterizedType(List::class.java, T::class.java)
            val adapter: JsonAdapter<T> = moshi.adapter(listType)
            val jsonFile = inputStreamToString(context.assets.open(jsonFileName))
            return adapter.fromJson(jsonFile)
        }
    }

}
