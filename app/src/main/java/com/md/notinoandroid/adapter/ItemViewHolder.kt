package com.md.notinoandroid.adapter

import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.LinearLayoutCompat
import androidx.recyclerview.widget.RecyclerView
import com.md.notinoandroid.R
import com.md.notinoandroid.api.UrlConstant.IMG_URL_PREFIX
import com.md.notinoandroid.entity.Product
import com.md.notinoandroid.glide.ImageLoader

abstract class BaseViewHolder(
    containerView: View
) : RecyclerView.ViewHolder(containerView) {
    abstract fun bindData(product: Product)
}

class ItemViewHolder(
    containerView: View, private val onClickListener: OnClickListener, imageLoader: ImageLoader
) : BaseViewHolder(containerView) {

    private val itemImgLoader: ImageLoader = imageLoader

    private val icHeart: AppCompatImageView by lazy {
        containerView.findViewById(R.id.ic_heart)
    }

    private val productImg: AppCompatImageView by lazy {
        containerView.findViewById(R.id.product_img)
    }

    private val textBrandName: TextView by lazy {
        containerView.findViewById(R.id.brand_name)
    }

    private val textProductName: TextView by lazy {
        containerView.findViewById(R.id.product_name)
    }

    private val textAnnotation: TextView by lazy {
        containerView.findViewById(R.id.product_annotation)
    }

    private val starsLayout: LinearLayoutCompat by lazy {
        containerView.findViewById(R.id.stars_layout)
    }

    private val textProductPrice: TextView by lazy {
        containerView.findViewById(R.id.product_price)
    }

    private val buttonBasket: AppCompatButton by lazy {
        containerView.findViewById(R.id.button_basket)
    }

    override fun bindData(product: Product) {

        /** Heart icon set State **/
        setActiveHeart()

        itemImgLoader.loadImage(IMG_URL_PREFIX + product.imageUrl, productImg)
        textBrandName.text = product.brand?.name
        textProductName.text = product.name

        textAnnotation.text = product.annotation

        buttonBasket.setOnClickListener {
            onClickListener.onClick(product)
        }

        /** Set Currency text **/
        setCurrencyText(product)

        /** Stars icon set State **/
        setActiveStar(product)
    }

    interface OnClickListener {
        fun onClick(product: Product)
    }

    private fun setActiveHeart(){
        var isActive = false
        icHeart.setOnClickListener {
            isActive = !isActive
            it.setBackgroundResource(if (!isActive) R.drawable.ic_heart_empty else R.drawable.ic_heart_fill)
        }
    }

    private fun setActiveStar(product: Product){
        val starScore = product.reviewSummary?.score
        if (starScore != null) {
            if (starScore > 0) {
                starsLayout.visibility = View.VISIBLE
                for (i in 0 until starScore) {
                    val star = starsLayout.getChildAt(i) as AppCompatImageView
                    star.setBackgroundResource(R.drawable.ic_star)
                }
            }
        }
    }

    private fun setCurrencyText(product: Product){
        var currencyValue: String? = null
        when(product.price?.currency){
            "CZK" -> {currencyValue = "KČ"}
            // TODO
        }

        textProductPrice.text = "${product.price?.value} ${currencyValue}"
    }
}