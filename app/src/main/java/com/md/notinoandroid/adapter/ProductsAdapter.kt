package com.md.notinoandroid.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.md.notinoandroid.R
import com.md.notinoandroid.entity.Product
import com.md.notinoandroid.glide.ImageLoader

class ProductsAdapter(
    private val onClickListener: OnClickListener,
    private val imageLoader: ImageLoader
) : RecyclerView.Adapter<ItemViewHolder>() {

    private var listItems = ArrayList<Product>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder {
        return ItemViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.product_item, parent, false),
            object : ItemViewHolder.OnClickListener {
                override fun onClick(product: Product) {
                    onClickListener.onItemClick(product)
                }
            }, imageLoader
        )
    }

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) {
        holder.bindData(listItems[position])
    }

    override fun getItemCount() = listItems.size

    interface OnClickListener {
        fun onItemClick(product: Product)
    }

    /** public method for use this adapter **/
    fun showList(products: List<Product>) {
        listItems.clear()
        listItems.addAll(products)
        notifyItemRangeInserted(0, listItems.size)
    }
}