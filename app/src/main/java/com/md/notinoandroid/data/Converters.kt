package com.md.notinoandroid.data

import androidx.room.TypeConverter
import com.md.notinoandroid.entity.*
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi

class Converters {

    @TypeConverter
    fun StringToBrand(json: String): Brand? {
        return getObjectFromString(json)
    }

    @TypeConverter
    fun BrandToString(brand: Brand): String? {
        return getStringFromObject(brand)
    }

    @TypeConverter
    fun StringToAttribute(json: String): Attribute? {
       return getObjectFromString(json)
    }

    @TypeConverter
    fun AttributeToString(attribute: Attribute?): String? {
        return getStringFromObject(attribute)
    }

    @TypeConverter
    fun StringToPackageSize(json: String): PackageSize? {
        return getObjectFromString(json)
    }

    @TypeConverter
    fun PackageSizeToString(packageSize: PackageSize?): String? {
        return getStringFromObject(packageSize)
    }

    @TypeConverter
    fun StringToPrice(json: String): Price? {
        return getObjectFromString(json)
    }

    @TypeConverter
    fun PriceToString(price: Price?): String? {
        return getStringFromObject(price)
    }

    @TypeConverter
    fun StringToReviewSummary(json: String): ReviewSummary? {
        return getObjectFromString(json)
    }

    @TypeConverter
    fun ReviewSummaryToString(reviewSummary: ReviewSummary?): String? {
        return getStringFromObject(reviewSummary)
    }

    @TypeConverter
    fun StringToStockAvailability(json: String): StockAvailability? {
        return getObjectFromString(json)
    }

    @TypeConverter
    fun StockAvailabilityToString(stockAvailability: StockAvailability?): String? {
        return getStringFromObject(stockAvailability)
    }

    /** Converter generic methods **/
    private inline fun<reified T> getObjectFromString(json: String): T?{
        val moshi: Moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<T> = moshi.adapter(T::class.java)
        val value = adapter.fromJson(json)
        return value
    }

    private inline fun<reified T> getStringFromObject(value: T): String?{
        val moshi: Moshi = Moshi.Builder().build()
        val adapter: JsonAdapter<T> = moshi.adapter(T::class.java)
        val valueString = adapter.toJson(value)
        return valueString
    }

}