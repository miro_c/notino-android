package com.md.notinoandroid.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.md.notinoandroid.entity.Product

@Dao
interface NotinoDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertData(data: List<Product>): List<Long>

    @Query("SELECT * FROM product_table ORDER BY productId ASC LIMIT 20")
    suspend fun getAllProducts(): List<Product>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(product: Product)

    @Query("DELETE FROM product_table")
    suspend fun deleteAll()
}