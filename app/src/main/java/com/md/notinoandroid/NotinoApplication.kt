package com.md.notinoandroid

import android.app.Application
import com.md.notinoandroid.db.NotinoRoomDatabase
import com.md.notinoandroid.repository.NotinoRepository

class NotinoApplication : Application() {
    val database by lazy { NotinoRoomDatabase.getDatabase(applicationContext) }
    val repository by lazy { NotinoRepository(database.notinoDao()) }
}