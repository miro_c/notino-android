package com.md.notinoandroid.entity

import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "product_table")
class Product(
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "productId") var productId: Long?,
    @Embedded var brand: Brand?,
    @Embedded var attributes: Attribute?,
    var annotation: String?,
    var masterId: Long?,
    var url: String?,
    var orderUnit: String?,
    @Embedded var price: Price?,
    var imageUrl: String?,
    @ColumnInfo(name = "productName") var name: String?,
    var productCode: String?,
    @Embedded var reviewSummary: ReviewSummary?,
    @Embedded var stockAvailability: StockAvailability?
) {
    constructor() : this(
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null,
        null
    )
}

data class Brand(
    var id: String?,
    @ColumnInfo(name = "name") var name: String?
) {
    constructor() : this(null, null)
}

data class Attribute(
    var new: Boolean?,
    var action: Boolean?,
    var differentPackaging: Boolean?,
    var master: Boolean?,
    var airTransportDisallowed: Boolean?,
    @Embedded var packageSize: PackageSize?,
    @ColumnInfo(name = "freeDelivery") var freeDelivery: Boolean?
) {
    constructor() : this(null, null, null, null, null, null, null)
}

data class PackageSize(
    var height: Int?,
    var width: Int?,
    @ColumnInfo(name = "depth") var depth: Int?
) {
    constructor() : this(null, null, null)
}

data class Price(
    var value: Int?,
    @ColumnInfo(name = "currency") var currency: String?
) {
    constructor() : this(null, null)
}

data class ReviewSummary(
    var score: Int?,
    @ColumnInfo(name = "averageRating") var averageRating: String?
) {
    constructor() : this(null, null)
}

data class StockAvailability(
    var code: String?,
    @ColumnInfo(name = "count") var count: Int?
) {
    constructor() : this(null, null)
}